﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
        [Required]
        public string Name { get; set; }
        public int EmployeeId { get; set; }
        public Employee? Employee { get; set; }
        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }

    }

}
