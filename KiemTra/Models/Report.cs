﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Report
    {
        [Key]
    public int ReportId { get; set; }
    public int AccountId { get; set; }
   
    public Account? Account { get; set; }
    public int LogId { get; set; }
    public Log? Log { get; set; }
    public int TransactionalId { get; set; }
    public Transaction? Transaction { get; set; }
        [Required]
        public string ReportName { get; set; }
        public DateTime? ReportDate { get; set; }
    }

}
