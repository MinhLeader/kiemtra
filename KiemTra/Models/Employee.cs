﻿using System.ComponentModel.DataAnnotations;
using System.Data;

namespace KiemTra.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        [Required, StringLength(50)]
        public string FirstName { get; set; }
        [Required, StringLength(50)]

        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Address {  get; set; }
        [Required]

        public string Username { get; set; }
        [Required]

        public string Password { get; set; }
        
    }

}
