﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        [Required, StringLength(50)]

        public string FirstName { get; set; }
        [Required, StringLength(50)]

        public string LastName { get; set; }
        public string Contact { get; set; }

        public string Address { get; set; }
        [Required]

        public string Username { get; set; }
        [Required]

        public string Password { get; set; }

    }

}
