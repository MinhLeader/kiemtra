﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Log
    {
        [Key] 
        public int LogId { get; set; }
        public int TransactionId { get; set; }
        public Transaction? Transaction { get; set; }
        public DateTime LogTime { get; set; }

        public DateTime LogDate { get; set; }
    }

}
